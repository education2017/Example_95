package hr.ferit.bruno.example_95;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by Zoric on 21.9.2017..
 */

public class ExitDialog extends DialogFragment {

    private static final String THANKS_MESSAGE = "Thank you for staying.";
    String mTitle = "Exit dialog";
    String mMessage = "Would you really like to leave this beautiful app?";
    String mPositiveButtonText = "Exit";
    String mNegativeButtonText = "Stay";
    
    DialogInterface.OnClickListener mPositiveButtonListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            getActivity().finish();
        }
    };

    DialogInterface.OnClickListener mNegativeButtonListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Toast.makeText(getActivity(), THANKS_MESSAGE,Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle(this.mTitle)
                .setMessage(this.mMessage)
                .setPositiveButton(this.mPositiveButtonText, this.mPositiveButtonListener)
                .setNegativeButton(this.mNegativeButtonText, this.mNegativeButtonListener);
        return builder.create();                
    }
}
