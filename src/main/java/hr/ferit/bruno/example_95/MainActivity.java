package hr.ferit.bruno.example_95;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends Activity {

    private final String  DIALOG_TAG = "ExitDialog";
    @BindView(R.id.bShowDialog) Button bShowDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bShowDialog)
    public void displayDialog(){
        ExitDialog exitDialog = new ExitDialog();
        exitDialog.show(getFragmentManager(), DIALOG_TAG);
    }
}

